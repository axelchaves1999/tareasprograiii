﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tarea01
{
    class Logica
    {
        internal int numeroSuerte(int num1,int num2,int num3)
        {

            String res = (num1 + num2 + num3).ToString();
            int total = 0;
            for (int i = 0; i < res.Length; i++)
            {    
                total += res[i] - '0';
            }
            return total;

        }
    }
}
