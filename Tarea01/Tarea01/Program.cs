﻿using System;

namespace Tarea01
{
    class Program
    {
        static void Main(string[] args)
        {
            static int LeerInt(string text) {
                int num = 0;
                do
                {
                    Console.WriteLine(text);
                } while (!Int32.TryParse(Console.ReadLine(),out num));
                return num;
                              
            }
            Logica log = new Logica();
            int dia = LeerInt("Ingrese dia de nacimiento: ");
            int mes = LeerInt("Ingrese mes de nacimiento: ");
            int ano = LeerInt("Ingrese año de nacimiento: ");
            Console.WriteLine("La fecha ingresada es {0}/{1}/{2}",dia,mes,ano);
            Console.WriteLine("Su numero de la suerte es: {0}",log.numeroSuerte(dia,mes,ano));
            Console.ReadKey();
        }
    }
}
