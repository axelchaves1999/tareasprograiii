﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio05
{
    class Logica
    {
        public LinkedList<Transportista> Transportistas { get; set; }

        public Logica()
        {
            Transportistas = new LinkedList<Transportista>();
            Rellenar();

        }


        /// <summary>
        /// Este metodo ingresa datos al sistema
        /// </summary>
        internal void Rellenar()
        {
            Transportistas.AddLast(new Transportista { Nombre = "Luis", Cedula = 123, Direccion = "La palmera", Telefono = "87670289" });
            Transportistas.AddLast(new Transportista { Nombre = "Gerardo", Cedula = 1234, Direccion = "Aguas Zarcas", Telefono = "87123089" });
            Transportistas.AddLast(new Transportista { Nombre = "Carlos", Cedula = 12345, Direccion = "La marina", Telefono = "69072289" });
            Transportistas.AddLast(new Transportista { Nombre = "Cristian", Cedula = 123456, Direccion = "Venecia", Telefono = "83452891" });
            Transportistas.AddLast(new Transportista { Nombre = "Josue", Cedula = 48092012, Direccion = "Santa Rosa", Telefono = "61252621" });
            Transportistas.AddLast(new Transportista { Nombre = "Henry", Cedula = 301239201, Direccion = "Pital", Telefono = "80012830" });
            Transportistas.AddLast(new Transportista { Nombre = "Andrey", Cedula = 603450421, Direccion = "Ciudad Quesada", Telefono = "8281298" });
            Transportistas.AddLast(new Transportista { Nombre = "Emanuel", Cedula = 101250121, Direccion = "Florencia", Telefono = "80412347" });
            Transportistas.AddLast(new Transportista { Nombre = "Anthony", Cedula = 303450221, Direccion = "Sucre", Telefono = "89346721" });

            Transportistas.ElementAt(0).Camiones.AddLast(new Camion { Placa = 2789123, TipoCamion = "Carga Pesada", CargaPeligrosa = false });
            Transportistas.ElementAt(1).Camiones.AddLast(new Camion { Placa = 187932, TipoCamion = "Carga Liviana", CargaPeligrosa = false });
            Transportistas.ElementAt(2).Camiones.AddLast(new Camion { Placa = 321223, TipoCamion = "Carga Ancha", CargaPeligrosa = true });
            Transportistas.ElementAt(3).Camiones.AddLast(new Camion { Placa = 1586123, TipoCamion = "Carga Pesada", CargaPeligrosa = true });
            Transportistas.ElementAt(4).Camiones.AddLast(new Camion { Placa = 486123, TipoCamion = "Carga Pesada", CargaPeligrosa = true });
            Transportistas.ElementAt(5).Camiones.AddLast(new Camion { Placa = 51586322, TipoCamion = "Carga Liviana", CargaPeligrosa = false });
            Transportistas.ElementAt(6).Camiones.AddLast(new Camion { Placa = 36786121, TipoCamion = "Carga Ancha", CargaPeligrosa = true });
            Transportistas.ElementAt(7).Camiones.AddLast(new Camion { Placa = 82556323, TipoCamion = "Carga Ancha", CargaPeligrosa = true });
            Transportistas.ElementAt(8).Camiones.AddLast(new Camion { Placa = 76566426, TipoCamion = "Carga Liviana", CargaPeligrosa = false });
        }

        /// <summary>
        /// Este metodo imprime todos los transportistas del sistema
        /// </summary>
        /// <returns>Devuelve un string con los datos de los transportistas</returns>
        internal string ImprimirLista()
        {
            string txt = "\n";
            int cont = 0;
            foreach (Transportista t in Transportistas)
            {
                cont++;
                txt += String.Format("{0}.Nombre:{1} Cédula: {2} Dirección: {3} Telefono: {4}\n" +
                    "Camiones: {5}\n", cont, t.Nombre, t.Cedula, t.Direccion, t.Telefono, t.ImpCamiones());
            }
            return txt;

        }
        
        /// <summary>
        /// Este metodo muestra la informacion de un transportista
        /// </summary>
        /// <param name="t">Este parametro es tipo Transportista
        /// y pertenece al transportista que desea editar</param>
        /// <returns>La informacion del transportista</returns>
        public string Mostrar(Transportista t)
        {
            if (t != null)
            {
                return String.Format("\nConcidencias:\n" +
                    "Nombre:{0} Cédula: {1}\n" +
                "Camiones: {2}\n", t.Nombre, t.Cedula, t.ImpCamiones());
            }
            return "No se encontro ninguna coincidencia";

        }

        /// <summary>
        /// Este metodo busca en el sistema un transportista con el numero de cedula ingresado
        /// </summary>
        /// <param name="cedula">Este parametro es tipo entero y pertenece al transportista que desea buscars</param>
        /// <returns>Devuelve un transportista</returns>
        public Transportista BusqTran(int cedula)
        {
            foreach (Transportista t in Transportistas)
            {
                if (t.Cedula == cedula)
                {
                    return t;
                }
            }
            return null;

        }

        /// <summary>
        /// Este metodo muestra la lista de camiones que posee un transportista
        /// </summary>
        /// <param name="t">Este paramtro es tipo transportista</param>
        /// <returns>Devuelve un string con los camiones que posee el transportista</returns>
        public string ListaCamiones(Transportista t)
        {
            string txt = "";
            for (int i = 0; i < t.Camiones.Count; i++)
            {
                Camion c = t.Camiones.ElementAt(i);
                txt += String.Format("{0}. Placa: {1} Tipo:{2} La carga es peligrosa: {3}",
                    i + 1, c.Placa, c.TipoCamion, c.CargaPeligrosa);
            }
            return txt;

        }

        /// <summary>
        /// Este metodo registra un transportista en el sistema
        /// </summary>
        /// <param name="t">Este parametro es tipo transportista y pertenece al transportista
        /// que desea registrar</param>
        /// <returns>Devuelve true si se logro registrar</returns>
        public bool Registrar(Transportista t)
        {
            if (t != null)
            {
                Transportistas.AddLast(t);
                return true;
            }
            return false;

        }
    }

}
