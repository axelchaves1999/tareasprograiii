﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace Ejercicio05
{
    class Transportista
    {

        public string Nombre { get; set; }
        public int Cedula { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }



        public LinkedList<Camion> Camiones { get; set; }

        public Transportista()
        {
            Camiones = new LinkedList<Camion>();
        }

        public string ImpCamiones()
        {
            string txt = "";
            foreach (Camion c in Camiones)
            {
                txt += String.Format("Placa: {0} Tipo de camion: {1} Carga Peligrosa: {2} \n",
                    c.Placa, c.TipoCamion, c.CargaPeligrosa);
            }
            return txt;

        }

    }
}
