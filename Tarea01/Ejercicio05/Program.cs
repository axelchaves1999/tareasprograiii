﻿using System;
using System.Linq;

namespace Ejercicio05
{
    class Program
    {   
        /// <summary>
        /// Este metodo permite leer un entero desde la consola
        /// </summary>
        /// <param name="txt">El texto que desea mostrar</param>
        /// <returns>El numero ingresado</returns>
        static int LeerInt(string txt)
        {
            int num = 0;
            do
            {
                Console.Write(txt);
            } while (!Int32.TryParse(Console.ReadLine(), out num));
            return num;

        }

        /// <summary>
        /// Este metodo permite leer un string desde consola
        /// </summary>
        /// <param name="txt">Este parametro es el string que desea mostrar</param>
        /// <returns>Devuelve la palabra ingresada </returns>
        static string LeerString(string txt)
        {
            string text = "";
            Console.Write(txt);
            return Console.ReadLine();
        }
        static void Main(string[] args)
        {
            Logica log = new Logica();

            string menuPrincipal = "\nEjercicio05 -UTN- v001\n" +
                "1.Busqueda de transportistas\n" +
                "2.Añadir nuevo transportista\n" +
                "3.Editar transportista\n" +
                "4.Eliminar transportista\n" +
                "5.Salir\n" +
                "Seleccione una opción: ";

            string menuBusqueda = "\nMenu de busqueda\n" +
                "1.Busqueda por cédula\n" +
                "2.Mostrar lista completa\n" +
                "3.Salir\n" +
                "Seleccione una una opción: ";

            Console.Write("----Bienvenido-----\n" +
                "1.Inicio\n" +
                "2.Salir\n" +
                "Seleccione una opción: ");
            if (Int32.Parse(Console.ReadLine()) == 2)
            {
                goto SALIR;
            }
            while (true)
            {
            PRINCIPAL:
                Console.Clear();
                int op = LeerInt(menuPrincipal);
                if (op == 1)
                {
                BUSQUEDA:
                    Console.Clear();
                    op = LeerInt(menuBusqueda);
                    switch (op)
                    {
                        case 1:
                            op = LeerInt("Ingrese número de cédula o -1 para salir: ");
                            if (op == -1)
                            {
                                goto BUSQUEDA;
                            }
                            Transportista t = log.BusqTran(op);
                            Console.WriteLine(log.Mostrar(t));
                            Console.ReadKey();
                            goto BUSQUEDA;
                        case 2:
                            Console.WriteLine(log.ImprimirLista());
                            Console.ReadKey();
                            goto BUSQUEDA;

                        case 3:
                            Console.Clear();
                            goto PRINCIPAL;
                    }

                }
                else if (op == 2)
                {
                    Console.WriteLine("\n---Menu añadir nuevo transportista---\n");
                    int cedula = LeerInt("Ingrese numero de cédula o -1 para salir: ");
                    if (cedula == -1)
                    {
                        goto PRINCIPAL;
                    }
                    string nombre = LeerString("Ingrese nombre: ");
                    Console.WriteLine("\nAgregar transporte\n");
                    int placa = LeerInt("Ingrese el numero de placa del camion: ");
                    Console.Write(String.Format("\n1.Carga pesada\n" +
                        "2.Carga liviana\n" +
                        "3.Carga ancha\n" +
                        "Seleccione una opcion: "));

                    string tipoCamion = Int32.Parse(Console.ReadLine()) == 1 ? "Carga pesada" :
                        Int32.Parse(Console.ReadLine()) == 2 ? "Carga liviana" : "Carga ancha";
                    Console.WriteLine("La carga es peligrosa\n" +
                        "1.Si\n" +
                        "2.No\n" +
                        "Seleccione una opciones: ");
                    bool peligrosa = Int32.Parse(Console.ReadLine()) == 1;
                    Transportista t = new Transportista { Cedula = cedula, Nombre = nombre };
                    t.Camiones.AddLast(new Camion { Placa = placa, TipoCamion = tipoCamion, CargaPeligrosa = peligrosa });
                    if (log.Registrar(t))
                    {
                        Console.WriteLine("Registro realizado con exito\n");
                        goto PRINCIPAL;
                    }
                    Console.WriteLine("Intentar nuevamente\n");
                    goto PRINCIPAL;
                }
                else if (op == 3)
                {
                EDITAR:
                    Console.Clear();
                    op = LeerInt(menuBusqueda);
                    switch (op)
                    {
                        case 1:
                            int cedula = LeerInt("Ingrese cédula o -1 para salir: ");
                            if (cedula == -1)
                            {
                                goto EDITAR;
                            }
                            Transportista t = log.BusqTran(cedula);
                            if (t == null)
                            {
                                Console.WriteLine("No se encontro ninguna concidencia");
                                Console.ReadKey();
                                goto EDITAR;
                            }
                            Console.WriteLine("\nNombre: {0} Cedula: {1}", t.Nombre, t.Cedula);
                            op = LeerInt("\n1.Para editar los datos personales del transportista\n" +
                                "2.Para editar los datos de los camiones\n" +
                                "3.Salir\n" +
                                "Seleccione una opciones: ");
                            if (op == 1)
                            {

                                Console.Clear();
                                op = LeerInt("\nMenu editar datos personales\n" +
                                    "1.Para editar la dirección de transportista\n" +
                                    "2.Para editar el telefono del transportista\n" +
                                    "Seleccione una opción: ");
                                if (op == 1)
                                {
                                    t.Direccion = LeerString("Ingrese la nueva dirección: ");
                                }
                                else if (op == 2)
                                {
                                    t.Telefono = LeerString("Ingrese el nuevo número de telefono: ");
                                }

                                Console.WriteLine("\n---Datos editados exitosamente---");
                                Console.ReadKey();
                                Console.Clear();

                            }
                            else if (op == 2)
                            {
                                op = LeerInt(log.ListaCamiones(t) + "\nSeleccione un camión: ");
                                Camion c = t.Camiones.ElementAt(op - 1);
                                op = LeerInt("\nMenu editar datos de camión\n" +
                                    "1.Para editar placa\n" +
                                    "2.Para editar tipo de camión\n" +
                                    "3.Para editar tipo de carga\n" +
                                    "Seleccione una opción: ");
                                if (op == 1)
                                {
                                    c.Placa = LeerInt("Ingrese la nueva placa: ");
                                }
                                else if (op == 2)
                                {
                                    Console.Write(String.Format("\n1.Carga pesada\n" +
                                    "2.Carga liviana\n" +
                                    "3.Carga ancha\n" +
                                     "Seleccione una opcion: "));
                                    string tipoCamion = Int32.Parse(Console.ReadLine()) == 1 ? "Carga pesada" :
                                    Int32.Parse(Console.ReadLine()) == 2 ? "Carga liviana" : "Carga ancha";
                                }
                                else if (op == 3)
                                {
                                    c.CargaPeligrosa = LeerInt("La carga es peligrosa\n" +
                                        "1.Si\n" +
                                        "2.No\n" +
                                        "Seleccione una opcion: ") == 1;
                                }
                                Console.WriteLine("\n---Camion editado con exito---");
                                Console.ReadKey();



                            }
                            Console.Clear();
                            break;
                        case 2:
                            op = LeerInt(log.ImprimirLista() + "\n" + "Ingrese el número de transportista que desea editar\n" +
                                "o -1 para salir: ");
                            if (op == -1) {
                                goto EDITAR;
                            }
                            t = log.Transportistas.ElementAt(op - 1);
                            if (t == null)
                            {
                                Console.WriteLine("No se encontro ninguna concidencia");
                                goto EDITAR;
                            }
                            Console.Clear();
                            Console.WriteLine("\nNombre: {0} Cedula: {1}", t.Nombre, t.Cedula);
                            op = LeerInt("\n1.Para editar los datos personales del transportista\n" +
                                "2.Para editar los datos de los camiones\n" +
                                "3.Salir\n" +
                                "Seleccione una opciones: ");
                            if (op == 1)
                            {
                                op = LeerInt("\nMenu editar datos personales\n" +
                                    "1.Para editar la dirección de transportista\n" +
                                    "2.Para editar el telefono del transportista\n" +
                                    "Seleccione una opciones: ");
                                if (op == 1)
                                {
                                    t.Direccion = LeerString("Ingrese la nueva dirección: ");
                                }
                                else if (op == 2)
                                {
                                    t.Telefono = LeerString("Ingrese el nuevo número de telefono: ");
                                }
                                Console.WriteLine("\n---Datos editados exitosamente---");
                                Console.ReadKey();
                                Console.Clear();

                            }
                            else if (op == 2)
                            {
                                op = LeerInt(log.ListaCamiones(t) + "\nSeleccione un camión: ");
                                Camion c = t.Camiones.ElementAt(op - 1);
                                op = LeerInt("\nMenu editar datos de camión\n" +
                                    "1.Para editar placa\n" +
                                    "2.Para editar tipo de camión\n" +
                                    "3.Para editar tipo de carga\n" +
                                    "Seleccione una opción: ");
                                if (op == 1)
                                {
                                    c.Placa = LeerInt("Ingrese la nueva placa: ");
                                }
                                else if (op == 2)
                                {

                                    Console.Write(String.Format("\n1.Carga pesada\n" +
                                    "2.Carga liviana\n" +
                                    "3.Carga ancha\n" +
                                    "Seleccione una opcion: "));

                                    string tipoCamion = Int32.Parse(Console.ReadLine()) == 1 ? "Carga pesada" :
                                        Int32.Parse(Console.ReadLine()) == 2 ? "Carga liviana" : "Carga ancha";
                                }
                                else if (op == 3)
                                {
                                    c.CargaPeligrosa = LeerInt("La carga es peligrosa\n" +
                                        "1.Si\n" +
                                        "2.No\n" +
                                        "Seleccione una opcion: ") == 1;
                                }
                                Console.WriteLine("\n---Camion editado con exito---");
                                Console.ReadKey();



                            }
                            Console.Clear();
                            break;

                        case 3:
                            goto PRINCIPAL;
                    }
                }
                else if (op == 4)
                {
                    Console.WriteLine(log.ImprimirLista());
                    op = LeerInt("Seleccione el numero de transportista que desea eliminar" +
                        " o ingrese -1 para salir: ");
                    if (op == -1)
                    {
                        Console.Clear();
                        goto PRINCIPAL;
                    }
                    log.Transportistas.Remove(log.Transportistas.ElementAt(op - 1));
                    Console.WriteLine(log.ImprimirLista());
                    goto PRINCIPAL;

                }
                else if (op == 5)
                {

                    goto SALIR;
                }
            }
        SALIR:
            Console.WriteLine("Gracias por usar la aplicación");
            Console.ReadKey();

        }
    }
}
