﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio02
{
    class Logica
    {
        internal string isPerfecto(int num)
        {
            int total = 0;
            string txt = "";
            for (int i = 1; i <=num ; i++)
            {
                if (num % i == 0) {
                    total += i;                 
                }
            }
            total -= num;
            if (total == num)
            {
                txt += "El número es perfecto";
            }
            else {
                txt += "El número no es perfecto";
            }
            return txt;
        }
    }
}
