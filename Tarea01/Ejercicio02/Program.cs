﻿using System;

namespace Ejercicio02
{
    class Program
    {
        static void Main(string[] args)
        {   
            static int LeerInt(string text) {
                int num = 0;
                do
                {
                    Console.WriteLine(text);
                } while (!Int32.TryParse(Console.ReadLine(),out num));
                return num;
            }

            Logica log = new Logica();
            while (true)
            {
                int num = LeerInt("Ingrese un numero: ");
                Console.WriteLine(log.isPerfecto(num));
                int res = LeerInt("Desea ingresar otro número\n" +
                    "1.Si\n" +
                    "2.No\n" +
                    "Respueta:");
                if (res == 2) {
                    goto SALIR;
                }
                Console.Clear();
            }
            SALIR:
            Console.WriteLine("Gracias por usar la aplicación");
            Console.ReadKey();
        }
            
    }
}
