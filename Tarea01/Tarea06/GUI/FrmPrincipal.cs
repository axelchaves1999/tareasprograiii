﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tarea_6.BO;
using Tarea_6.Entidades;

namespace Tarea_6
{
    public partial class FrmPrincipal : Form
    {
        private Persona per;
        public FrmPrincipal()
        {
            InitializeComponent();
            per = new Persona();

        }
        private void CargarDatos(string filtro)
        {
            data.Rows.Clear();
            foreach (Persona p in new PersonaBO().Seleccionar(filtro))
            {
                if (p != null)
                {
                    data.Rows.Add(p, p.Cedula, p.Nombre, p.Direccion, p.Telefono, p.FechaNac, p.Activo);
                }
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                per.Cedula = txtCed.Text.Trim(); ;
                per.Nombre = txtNombre.Text.Trim();
                per.Direccion = txtDir.Text.Trim();
                per.Telefono = txtTel.Text.Trim();
                per.FechaNac = txtFech.Text.Trim();

                if (new PersonaBO().Insertar(per))
                {
                    MessageBox.Show("Registrado con exito");
                    CargarDatos(txtFiltro.Text.Trim());
                    LimiparDatos();
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            string fecha1 = txtFecha1.Text.Trim();
            string fecha2 = txtFech2.Text.Trim();
            data.Rows.Clear();
            foreach (Persona p in new PersonaBO().SeleccionarFech(fecha1, fecha2))
            {
                if (p != null)
                {
                    data.Rows.Add(p, p.Cedula, p.Nombre, p.Direccion, p.Telefono, p.FechaNac, p.Activo);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string filtro = txtFiltro.Text.Trim();
            if (String.IsNullOrEmpty(filtro))
            {
                var res = MessageBox.Show("¿Seguro que desea cargar todos los elementos?", "Información",
                    MessageBoxButtons.YesNo);
                if (res == DialogResult.No)
                {
                    return;
                }

            }
            CargarDatos(filtro);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            per = (Persona)(data.CurrentRow.Cells[0].Value);
            var res = MessageBox.Show(String.Format("¿Seguro que desea activar/desactivar a {0}?", per.Nombre), "Información",
                MessageBoxButtons.YesNo);
            if (res == DialogResult.No)
            {
                return;
            }
            if (per != null && new PersonaBO().ActivarDesactivar(per))
            {
                CargarDatos(txtFiltro.Text.Trim());
            }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            per = (Persona)data.CurrentRow.Cells[0].Value;
            if (per != null)
            {
                CargarData();

            }
        }

        private void CargarData()
        {
            txtCed.Text = per.Cedula;
            txtNombre.Text = per.Nombre;
            txtDir.Text = per.Direccion;
            txtTel.Text = per.Telefono;
            txtFech.Text = per.FechaNac;
            txtFech.Enabled = false;
            txtCed.Enabled = false;
            txtNombre.Enabled = false;
        }
        private void LimiparDatos()
        {
            txtCed.Enabled = true;
            txtNombre.Enabled = true;
            txtFech.Enabled = true;
            txtCed.Text = "";
            txtNombre.Text = "";
            txtDir.Text = "";
            txtTel.Text = "";
            txtFech.Text = "";
        }

        private void txtFiltro_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtFiltro_KeyUp(object sender, KeyEventArgs e)
        {
            string filtro = txtFiltro.Text.Trim();
            if (filtro.Length > 2)
            {
                CargarDatos(txtFiltro.Text);

            }
            else
            {
                data.Rows.Clear();

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            LimiparDatos();
        }
    }
}
