﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarea_6.Entidades;
using Npgsql;
using System.Data.SqlTypes;
using System.Windows.Forms;

namespace Tarea_6.DAO
{
    public class PersonaDAO
    {
        public NpgsqlConnection con;


        public void Conexion()
        {
            con = new NpgsqlConnection("Server = localhost; User id = postgres; Password = Chaves123; Database = Prueba");

        }
        public bool Insertar(Persona p)
        {
            Conexion();
            con.Open();
            string sql = "INSERT INTO public.personas(cedula, nombre, direccion, telefono, fecha_nac) " +
                " VALUES(@ced, @nom, @dir, @tel, @fech); ";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@ced", p.Cedula);
            cmd.Parameters.AddWithValue("@nom", p.Nombre);
            cmd.Parameters.AddWithValue("@dir", p.Direccion);
            cmd.Parameters.AddWithValue("@tel", p.Telefono);
            cmd.Parameters.AddWithValue("@fech", Convert.ToDateTime(p.FechaNac));
            return cmd.ExecuteNonQuery() == 1;

        }

        public bool Editar(Persona p) {
            Conexion();
            con.Open();
            string sql = "update personas set direccion = @dir,telefono = @tel where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@dir", p.Direccion);
            cmd.Parameters.AddWithValue("@tel", p.Telefono);
            cmd.Parameters.AddWithValue("@id", p.Id);
            return cmd.ExecuteNonQuery() == 1;
        }

        public bool ActivarDesactivar(Persona p) {
            Conexion();
            con.Open();
            string sql = "update personas set activo = @act where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@act",p.Activo == true? false:true);
            cmd.Parameters.AddWithValue("@id",p.Id);
            return cmd.ExecuteNonQuery() == 1;
        }

        public List<Persona> Seleccionar(string filtro)
        {

            List<Persona> personas = new List<Persona>();
            Conexion();
            con.Open();
            string sql = "select id,cedula,nombre,direccion,telefono,fecha_nac,activo from personas \n" +
                " where cedula like @ced or lower(nombre) like lower(@nom) order by cedula";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@ced", filtro + '%');
            cmd.Parameters.AddWithValue("@nom", filtro + '%');
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    personas.Add(Cargar(reader));
                }
            }
         
            return personas;

        }

        public List<Persona> SeleccionarFech(string fech1, string fech2)
        {
            try
            {
                Conexion();
                con.Open();
                List<Persona> personas = new List<Persona>();
                string sql = "select id,cedula,nombre,direccion,telefono,fecha_nac,activo from personas " +
                    " where fecha_nac between @fech1 and @fech2  ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@fech1", Convert.ToDateTime(fech1));
                cmd.Parameters.AddWithValue("@fech2",Convert.ToDateTime(fech2));

                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        personas.Add(Cargar(reader));
                    }
                }
                return personas;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            return null;

        }

        private Persona Cargar(NpgsqlDataReader reader)
        {
            try
            {
                Persona p = new Persona
                {
                    Id = reader.GetInt32(0),
                    Cedula = reader.GetString(1),
                    Nombre = reader.GetString(2),
                    Direccion = reader.GetString(3),
                    Telefono = reader.GetString(4),
                    FechaNac = Convert.ToString(reader.GetDate(5)),
                    Activo = reader.GetBoolean(6)
                    

                };
                return p;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            return null;


        }
    }
}
