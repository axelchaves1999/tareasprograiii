﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Tarea_6.Entidades
{
    public class Persona
    {
        public int Id { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string FechaNac { get; set; }

        public bool Activo { get; set; }

        public override string ToString()
        {
            return String.Format("Nombre: {0}, Cédula: {1}\n" +
                "Dirección: {2}, Teléfono: {3}\n" +
                "Fecha Nacimiento: {4}",Nombre,Cedula,Direccion,Telefono,FechaNac);
        }


    }
}
