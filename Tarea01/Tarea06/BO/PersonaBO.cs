﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tarea_6.DAO;
using Tarea_6.Entidades;

namespace Tarea_6.BO
{
    public class PersonaBO
    {
        public bool Insertar(Persona p)
        {

            if (String.IsNullOrEmpty(p.Cedula))
            {
                throw new Exception("Necesita ingresar cédula");
            }
            if (String.IsNullOrEmpty(p.Nombre))
            {
                throw new Exception("Necesita ingresar nombre");
            }
            if (String.IsNullOrEmpty(p.Telefono))
            {
                throw new Exception("Necesita ingresar Teléfono");
            }
            if (String.IsNullOrEmpty(p.Direccion))
            {
                throw new Exception("Necesita ingresar dirección");
            }
            if (String.IsNullOrEmpty(p.FechaNac))
            {
                throw new Exception("Necesita ingresar fecha de nacimiento");
            }
            if (p.Id <= 0)
            {
                return new PersonaDAO().Insertar(p);

            }
            else {
                return new PersonaDAO().Editar(p);
            }
        }

        public List<Persona> Seleccionar(string filtro) {
            return new PersonaDAO().Seleccionar(filtro);
        }

        public List<Persona> SeleccionarFech(string fech1,string fecha2) {

            return new PersonaDAO().SeleccionarFech(fech1, fecha2);
        }

        public bool ActivarDesactivar(Persona p) {
            return new PersonaDAO().ActivarDesactivar(p);
        }
    }
}
