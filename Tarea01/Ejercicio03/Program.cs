﻿using System;
using System.Runtime;
using System.Runtime.InteropServices.ComTypes;

namespace Ejercicio03
{
    class Program
    {
        static void Main(string[] args)
        {
            static int LeerInt(string txt)
            {
                int num = 0;
                do
                {
                    Console.WriteLine(txt);
                } while (!Int32.TryParse(Console.ReadLine(), out num));
                return num;
            }
         
            int rango = LeerInt("Ingrese el rango de números que desea: ");
            int intentos = LeerInt("Ingrese el numero de intentos que desea: ");
            int random = new Random().Next(0,rango);
            for (int i = 0; i < intentos; i++)
            {
                int intento = LeerInt("Ingrese un número: ");
                if (intento > random)
                {
                    Console.WriteLine("El número es menor");
                }
                if (intento < random)
                {
                    Console.WriteLine("El número es mayor");
                }
                if (intento == random)
                {
                    Console.WriteLine("Felicidades gano");
                    break;
                }
            }
            Console.WriteLine("Perdio");

        }
    }
}
