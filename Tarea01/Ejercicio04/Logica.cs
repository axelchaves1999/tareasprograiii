﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio04
{
    class Logica
    {
        internal string Riman(string primera, string segunda)
        {
            if (primera.Substring(primera.Length - 3, 3).Equals(segunda.Substring(segunda.Length - 3, 3))) {
                return "Las palabras riman";
            };
            if (primera.Substring(primera.Length - 2, 2).Equals(segunda.Substring(segunda.Length - 2, 2))) {
                return "las palabras solo riman un poco";
            }

            return "No rima"; 
        }
    }
}
