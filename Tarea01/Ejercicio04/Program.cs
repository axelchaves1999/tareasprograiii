﻿using System;

namespace Ejercicio04
{
    class Program
    {
        static void Main(string[] args)
        {
            static int LeerInt(string txt) {
                int num = 0;
                do
                {
                    Console.WriteLine(txt);
                } while (!Int32.TryParse(Console.ReadLine(),out num));
                return num;
            }


            Logica log = new Logica();
            while (true)
            {
                Console.WriteLine("Ingrese la primera palabra");
                string primera = Console.ReadLine();
                Console.WriteLine("Ingrese la segunda palabra");
                string segunda = Console.ReadLine();
                Console.WriteLine(log.Riman(primera, segunda));

                int op = LeerInt("Desea ingresar otra palabra\n" +
                    "1.Si\n" +
                    "2.No\n" +
                    "Seleccione una opción: ");
                if(op == 2){
                    break;
                }
            }
            
        }
    }
}
